package vexillum.sqliteconnector;

import android.database.Cursor;
import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    String fName, lName;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db=openOrCreateDatabase("MyDB1" ,MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS Gamers(fname VARCHAR, lname VARCHAR);");
    }

    public void AddData(View view)
    {
        EditText editText1 = (EditText)findViewById(R.id.inputFirst);
        EditText editText2 = (EditText)findViewById(R.id.inputLast);

        fName = editText1.getText().toString();
        lName = editText2.getText().toString();
        db.execSQL("INSERT INTO Gamers VALUES('"+fName+"','"+lName+"');");
    }

    public void ShowData(View view)
    {
        Cursor c = db.rawQuery("SELECT * FROM Gamers", null);
        int count = c.getCount();
        c.moveToFirst();
        TableLayout tableLayout = new TableLayout(getApplicationContext());
        tableLayout.setVerticalScrollBarEnabled(true);
        TableRow tableRow;
        TextView textView, textView1, textView2, textView3;
        tableRow = new TableRow(getApplicationContext());
        textView = new TextView(getApplicationContext());
        textView.setText("First Name");
        textView.setTextColor(Color.RED);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setPadding(20, 20, 20, 20);
        tableRow.addView(textView);

        textView1 = new TextView(getApplicationContext());
        textView1.setText("Last Name");
        textView1.setTextColor(Color.RED);
        textView1.setTypeface(null, Typeface.BOLD);
        textView1.setPadding(20, 20, 20, 20);
        tableRow.addView(textView1);

        for (Integer i = 0; i < count; i++)
        {
            tableRow = new TableRow(getApplicationContext());
            textView2 = new TextView(getApplicationContext());
            textView2.setText(c.getString(c.getColumnIndex("fname")));
            textView3 = new TextView(getApplicationContext());
            textView3.setText(c.getString(c.getColumnIndex("lname")));
            textView2.setPadding(20, 20, 20, 20);
            textView3.setPadding(20, 20, 20, 20);
            tableRow.addView(textView2);
            tableRow.addView(textView3);
            tableLayout.addView(tableRow);
            c.moveToNext();
        }

        setContentView(tableLayout);
        db.close();

    }

    public void Close(View view)
    {
        System.exit(0);
    }


}
